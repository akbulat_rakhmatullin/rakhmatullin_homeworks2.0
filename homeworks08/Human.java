public class Human {

    private String name; // имя

    private double weight;// вес


    public  String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        if (weight < 0 || weight > 350){
            weight = 0;
        }
        this.weight = weight;
    }

    public double getWeight() {
        return this.weight;
    }
}
