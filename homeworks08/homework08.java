import java.util.Arrays;
import java.util.Scanner;
public class homework08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Human[] humans = new Human[10];
        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            System.out.println("Введите имя " + (i + 1) + ": ");
            String name = scanner.nextLine();
            humans[i].setName(name);
            System.out.println("Введите вес " + (i + 1) + ": ");
            double weight = Double.parseDouble(scanner.nextLine());
            humans[i].setWeight(weight);
        }
        sortWeight(humans);

    }

    public static void sortWeight(Human[] array) {
        for (int i = 0; i < array.length; i++) {
            double minWeight = array[i].getWeight();
            int minWeightIndex = i;
            for( int j = i; j < array.length; j++) {
                if (array[j].getWeight() < minWeight) {
                    minWeight = array[j].getWeight();
                    minWeightIndex = j;
                }
                Human temp = array[i];
                array[i] = array[minWeightIndex];
                array[minWeightIndex] = temp;
            }
            System.out.println(array[i].getName() + " " + array[i].getWeight()) ;

        }
    }
    }

