package homework11;

public class Logger {

    private static final Logger logger;

    static {
        logger = new Logger();
    }

    public static Logger getInstance() {
        return logger;
    }

    private Logger() {
        this.message = new String();
    }

    private String message;

    void log(String message) {
        System.out.println(message);
    }

}