public class Ellips extends Figure{
    private final double  radius1;
    private final double  radius2;

    public Ellips(double  coordinateX, double  coordinateY, double  radius1, double  radius2) {
        super(coordinateX, coordinateY);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public double getPerimetr() {
        return 4 * (Math.PI * radius1 * radius2 + (radius1 - radius2) * (radius1 - radius2)) / (radius1 + radius2);
    }
}
