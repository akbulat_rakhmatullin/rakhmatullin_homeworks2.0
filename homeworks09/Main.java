public class Main {

    public static void main(String[] args) {
        Figure figure = new Figure(3,4);
        Rectangle rectangle = new Rectangle(32,4,5,5);
        Square square = new Square(3,8,9);
        Ellips ellips = new Ellips(4,3,7,7);
        Circle circle = new Circle(4,5,6);

        System.out.println(rectangle.getPerimetr());
        System.out.println(square.getPerimetr());
        System.out.println(ellips.getPerimetr());
        System.out.println(circle.getPerimetr());
        System.out.println(figure.getPerimeter());
        
    }
}
