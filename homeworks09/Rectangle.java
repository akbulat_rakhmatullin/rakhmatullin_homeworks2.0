public class Rectangle extends Figure{
    private final double a;
    private final double b;

    public Rectangle(double coordinateX, double coordinateY, double a, double b) {
        super(coordinateX, coordinateY);
        this.a = a;
        this.b = b;
    }

    public double  getPerimetr(){
        return (a + b) * 2;
    }
}
